package main

import (
	"context"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/dkolbly/sprinklerd/spec"
)

// a Schedule is a single, named event.  It runs independently of all
// the other schedules, and contributes events to the system
type Schedule struct {
	source *spec.Event
	update chan<- *spec.Event
	loc    *time.Location
}

// NewSchedule builds a new schedule from the operational spec
func NewSchedule(in *time.Location, from *spec.Event, to chan<- transition) *Schedule {
	up := make(chan *spec.Event)
	sched := &Schedule{
		source: from,
		update: up,
		loc:    in,
	}
	go sched.run(to, up)
	return sched
}

func (sched *Schedule) Cancel() {
	close(sched.update)
}

func (sched *Schedule) Update(ev *spec.Event) {
	if ev.EnableDaysOfWeek == 0 {
		panic("should have canceled instead")
	}
	sched.update <- ev
}

type tick <-chan time.Time

func tform(t time.Time) string {
	return t.Format("Mon Jan 2 15:04:05 -0700")
}

func until(t time.Time) time.Duration {
	return time.Until(t).Round(time.Second)
}

func (sched *Schedule) next(ctx context.Context, t time.Time) (tick, tick, bool) {
	nextStart := sched.source.Next(ctx, t)
	// we should never be called with an empty schedule
	if nextStart == nil {
		panic("schedule should have been canceled instead")
	}
	log.Debugf(ctx, "OK, next start after %s is at %s, %s from now",
		tform(t), tform(*nextStart), until(*nextStart))

	nextEnd := sched.source.NextEnd(ctx, t)
	if nextEnd == nil {
		// this should have panic()'d after NextEnd
		panic("should never have gotten here")
	}

	log.Debugf(ctx, "OK, next end is at %s, %s from now",
		tform(*nextEnd), until(*nextEnd))

	return time.After(nextStart.Sub(t)), time.After(nextEnd.Sub(t)), nextEnd.Before(*nextStart)
}

func (sched *Schedule) run(tx chan<- transition, up <-chan *spec.Event) {
	ctx := logging.Set(context.Background(), "ev", sched.source.Name)

	var start, end tick
	var in bool
	sent := false

	t0 := time.Now().In(sched.loc)
	start, end, in = sched.next(ctx, t0)
	if in {
		log.Debugf(ctx, "initially in")
		tx <- transition{
			at:      t0,
			relays:  0xffff, // enable all relays
			subject: sched.source,
			state:   true,
		}
		sent = true
	}

	for {
		// either this schedule will get updated (including
		// canceled), or we will start, or we will end.
		select {
		case change, ok := <-up:
			if !ok {
				log.Debugf(ctx, "schedule deleted")
				if sent {
					tx <- transition{
						at:      time.Now().In(sched.loc),
						relays:  0xffff, // clear all relays
						subject: sched.source,
						state:   false,
					}
				}
				return
			} else {
				log.Debugf(ctx, "schedule update: %#v", change)
				sched.source = change
				now := time.Now().In(sched.loc)
				start, end, in = sched.next(ctx, now)
				log.Debugf(ctx, "in=%t sent=%t", in, sent)

				if in != sent {
					log.Debugf(ctx, "in=%t but sent=%t", in, sent)
					tx <- transition{
						at:      now,
						relays:  0xffff, // touch all relays
						subject: sched.source,
						state:   in,
					}
					sent = in
				}
			}

		case t := <-start:
			log.Debugf(ctx, "schedule start at %s",
				t.Format("Mon Jan 2 15:04:05"))
			tx <- transition{
				at:      t,
				relays:  0xf,
				subject: sched.source,
				state:   true,
			}
			sent = true
			start, _, _ = sched.next(ctx, t)

		case t := <-end:
			log.Debugf(ctx, "scheduled end at %s",
				t.Format("Mon Jan 2 15:04:05"))
			tx <- transition{
				at:      t,
				relays:  0xf,
				subject: sched.source,
				state:   false,
			}
			sent = false
			_, end, _ = sched.next(ctx, t)
		}
	}

}
