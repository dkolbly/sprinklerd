//+build linux arm

package main

import (
	"bitbucket.org/dkolbly/gokrazy/lib/i2c"
)

type Driver struct {
	dev *i2c.Device
}

func NewDriver() (*Driver, error) {
	bus, err := i2c.New(0x20, 1)
	if err != nil {
		return nil, err
	}
	return &Driver{dev: bus}, nil
}

func (d *Driver) Set(state uint8) error {
	_, err := d.dev.Write([]byte{0x06, 0xff ^ state})
	return err
}
