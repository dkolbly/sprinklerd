//+build gokrazy

package main

//go:generate go-bindata -o zoneinfo-bindata.go -prefix /usr/local/go /usr/local/go/lib/time/zoneinfo.zip

import (
	"context"
	"io/ioutil"
	"os"

	"github.com/gokrazy/gokrazy"
)

func Startup(ctx context.Context) {
	log.Debugf(ctx, "Waiting for clock")
	gokrazy.WaitForClock()

	data := MustAsset("lib/time/zoneinfo.zip")
	log.Debugf(ctx, "Writing out zoneinfo (%d bytes)", len(data))

	ioutil.WriteFile("/tmp/zoneinfo.zip", data, 0644)
	os.Setenv("ZONEINFO", "/tmp/zoneinfo.zip")

	log.Debugf(ctx, "Ready to rock")
}
