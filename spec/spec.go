package spec

//go:generate protoc -I . spec.proto --go_out=.

import (
	"context"
	"time"

	"bitbucket.org/dkolbly/logging"
)

var log = logging.New("spec")

func sundayNoon(t time.Time) time.Time {
	yy, mm, dd := t.Date()
	noon := time.Date(yy, mm, dd, 12, 0, 0, 0, t.Location())
	// bring it back to sunday
	return noon.AddDate(0, 0, -int(noon.Weekday()))
}

// Next computes the next time after the given time that this event
// will fire, or nil if it won't fire again.  Note that the *Location
// of the given time is important, because it determines when noon local time
// is and configures daylight savings as well
func (e *Event) Next(ctx context.Context, t time.Time) *time.Time {
	ctx = logging.Set(ctx, "ev", e.Name)
	log.Debugf(ctx,
		"checking next %q time after %s",
		e.Name,
		t.Format("2006-01-02 15:04:05 -0700"),
	)
	if e.EnableDaysOfWeek == 0 {
		log.Debugf(ctx, "all days are disabled")
		return nil
	}
	up := e.nedge(ctx, t, false)
	return &up
}

func (e *Event) NextEnd(ctx context.Context, t time.Time) *time.Time {
	ctx = logging.Set(ctx, "ev", e.Name)
	log.Debugf(ctx,
		"checking next %q end time after %s",
		e.Name,
		t.Format("2006-01-02 15:04:05 -0700"),
	)
	if e.EnableDaysOfWeek == 0 {
		log.Debugf(ctx, "all days are disabled")
		return nil
	}
	up := e.nedge(ctx, t, true)
	return &up
}

func (e *Event) nedge(ctx context.Context, t time.Time, end bool) time.Time {
	weekOrigin := sundayNoon(t)

	// if there are any days enabled, then we should be able
	// to find one before next year...
	best := t.AddDate(1, 0, 0)

	offset := time.Second * time.Duration(e.StartTimeOfDay)
	if end {
		offset += time.Second * time.Duration(e.DurationSec)
	}

	for i := 0; i < 7; i++ {
		dow := time.Weekday(i)
		if e.EnableDaysOfWeek&(1<<uint(i)) == 0 {
			log.Debugf(ctx, "%s is disabled", dow)
			continue
		}
		ev := weekOrigin.AddDate(0, 0, i).Add(offset)
		if ev.Before(t) {
			// it's before now... next week should
			// be fine though
			ev = ev.AddDate(0, 0, 7)
		}
		log.Debugf(ctx, "%s at %s",
			dow,
			ev.Format("Mon Jan 2 15:04"))
		if ev.Before(best) {
			best = ev
		}
	}

	return best
}

// determine if we are currently in the given event, which is the case
// if the next transition for this event is an END *and* the START
// corresponding to that END is before the current time
func (e *Event) In(ctx context.Context, t time.Time) bool {
	if e.EnableDaysOfWeek == 0 {
		return false
	}

	end := e.nedge(ctx, t, true)
	begin := end.Add(-time.Second * time.Duration(e.DurationSec))
	return begin.Before(t)
}
