package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/dkolbly/sprinklerd/spec"
	"github.com/dkolbly/cli"
	"github.com/golang/protobuf/proto"
	"gopkg.in/yaml.v2"
)

var log = logging.New("mss")

func main() {
	logging.SetHumanOutput(false, false, "DEBUG")

	app := &cli.App{
		Name:   "make-sprinkler-schedule",
		Usage:  "create a sprinkler system schedule",
		Action: doCompile,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "input",
				Aliases: []string{"i"},
				Usage:   "input YAML file",
				Value:   "input.yml",
			},
			&cli.StringFlag{
				Name:    "output",
				Aliases: []string{"o"},
				Usage:   "output protobuf file",
				Value:   "schedule.pb",
			},
		},
	}
	if app.Run(os.Args) != nil {
		os.Exit(1)
	}
}

func doCompile(c *cli.Context) error {
	var sys System
	ctx := context.Background()

	srcFile := c.String("input")
	dstFile := c.String("output")

	src, err := ioutil.ReadFile(srcFile)
	if err != nil {
		log.Errorf(ctx, "Could not read input: %s", err)
		return err
	}

	err = yaml.Unmarshal(src, &sys)
	if err != nil {
		log.Errorf(ctx, "%s: Could not parse: %s", srcFile, err)
		return err
	}
	log.Debugf(ctx, "OK %#v", sys)

	good := true

	// figure out the timezone (location) where the schedule should operate
	if sys.Location == "" {
		sys.Location = inferLocation()
		log.Warningf(ctx, "%s: no `location` specified, assuming %s",
			srcFile,
			sys.Location)
	}

	out := &spec.System{
		Location: sys.Location,
		Events: make(map[string]*spec.Event),
	}
	
	// parse the individual schedules
	for _, sch := range sys.Schedules {
		ev, ok := sch.parse(ctx)
		if !ok {
			good = false
			continue
		}
		if _, exists := out.Events[sch.Name]; exists {
			log.Errorf(ctx, "schedule name %q already defined", sch.Name)
			good = false
		}
		out.Events[sch.Name] = ev
	}
	if !good {
		return fmt.Errorf("invalid input file")
	}

	buf, err := proto.Marshal(out)
	if err != nil {
		panic(err) // should not happen
	}

	fd, err := os.Create(dstFile)
	if err != nil {
		log.Errorf(ctx, "Could not create output: %s", err)
		return err
	}
	defer fd.Close()

	fd.Write(buf)
	return nil
}

type System struct {
	Location  string
	Schedules []Schedule
}

type Schedule struct {
	Name     string
	Days     string
	Start    string
	End      string
	Duration string
	Circuit  int
}

var dowmap = map[string]uint{
	"sun": 0,
	"mon": 1,
	"tue": 2,
	"wed": 3,
	"thu": 4,
	"fri": 5,
	"sat": 6,
	"su":  0,
	"mo":  1,
	"tu":  2,
	"we":  3,
	"th":  4,
	"fr":  5,
	"sa":  6,
	"m":   1,
	"w":   3,
	"f":   5,
}

func parsedow(ctx context.Context, s string) (uint32, bool) {
	var dow uint32

	for _, chunk := range strings.Split(strings.ToLower(s), ",") {
		ranges := strings.SplitN(chunk, "-", 2)
		if len(ranges) == 1 {
			i, ok := dowmap[ranges[0]]
			if !ok {
				return 0, false
			}
			log.Debugf(ctx, "include %s", time.Weekday(i))
			dow |= 1 << i
		} else {
			i, ok := dowmap[ranges[0]]
			if !ok {
				return 0, false
			}
			j, ok := dowmap[ranges[1]]
			if !ok {
				return 0, false
			}
			// go from i to j, looping around.  That lets us do things
			// like go from friday to monday
			for {
				log.Debugf(ctx, "include %s", time.Weekday(i))
				dow |= 1 << i
				if i == j {
					break
				}
				i = (i + 1) % 7
			}
		}
	}
	return dow, true
}

func parseduration(ctx context.Context, s string) (uint32, bool) {

	parts := durformint.FindStringSubmatch(s)
	if parts != nil {
		qty, _ := strconv.ParseUint(parts[1], 10, 16)
		switch parts[2] {
		case "h":
			return uint32(qty) * 3600, true
		case "m":
			return uint32(qty) * 60, true
		default:
			panic("bad match")
		}
	}

	parts = durformhhmm.FindStringSubmatch(s)
	if parts != nil {
		hh, _ := strconv.ParseUint(parts[1], 10, 16)
		mm, _ := strconv.ParseUint(parts[2], 10, 16)
		return uint32(hh)*3600 + uint32(mm)*60, true
	}

	log.Errorf(ctx, "duration %q is neither `Nh` (hours), `Nm` (minutes), nor `h:mm`", s)
	return 0, false
}

var durformint = regexp.MustCompile(`^([1-9][0-9]*)([hm])$`)
var durformhhmm = regexp.MustCompile(`^([0-9]*):([0-5][0-9])$`)

var timeform24h = regexp.MustCompile(`^([012]?[0-9]):([0-5][0-9])$`)
var timeform12h = regexp.MustCompile(`^(10|11|12|[1-9]):([0-5][0-9]) ([AaPp][Mm])$`)

func parsetod(ctx context.Context, s string) (int32, bool) {
	parts := timeform24h.FindStringSubmatch(s)
	if parts != nil {
		hh, _ := strconv.ParseUint(parts[1], 10, 16)
		mm, _ := strconv.ParseUint(parts[2], 10, 16)
		log.Debugf(ctx, "%d %d", hh, mm)
		return int32(hh)*3600 + int32(mm)*60 - 12*3600, true
	}

	parts = timeform12h.FindStringSubmatch(s)
	if parts != nil {
		log.Debugf(ctx, "%#q", parts)
		hh, _ := strconv.ParseUint(parts[1], 10, 16)
		mm, _ := strconv.ParseUint(parts[2], 10, 16)
		log.Debugf(ctx, "%d %d %s", hh, mm, parts[3])
		return int32(hh)*3600 + int32(mm)*60 - 12*3600, true
	}

	log.Errorf(ctx, "%q is neither `HH:MM` 24h format nor `H:MM (AM|PM)` format", s)
	return 0, false
}

func (s Schedule) parse(ctx context.Context) (*spec.Event, bool) {
	var dow uint32
	var start int32
	var duration uint32

	if s.Name == "" {
		log.Errorf(ctx, "no `name` supplied")
		return nil, false
	}

	if s.Start == "" {
		log.Errorf(ctx, "must supply a `start` time")
		return nil, false
	}

	if s.End == "" && s.Duration == "" {
		log.Errorf(ctx, "must supply either a `duration` or an `end` time")
		return nil, false
	}

	if s.End != "" && s.Duration != "" {
		log.Errorf(ctx, "cannot supply both a `duration` and an `end` time")
		return nil, false
	}

	start, ok := parsetod(ctx, s.Start)
	if !ok {
		return nil, false
	}

	if s.End != "" {
		e, ok := parsetod(ctx, s.End)
		if !ok {
			return nil, false
		}
		if e < start {
			log.Errorf(ctx, "end time `%s` is before start time `%s`", s.End, s.Start)
			return nil, false
		}
		duration = uint32(e - start)
		log.Debugf(ctx, "duration is %d", duration)
	} else {
		d, ok := parseduration(ctx, s.Duration)
		if !ok {
			return nil, false
		}
		duration = d
	}

	if s.Days == "" {
		dow = 0x7f
	} else {
		dow, ok = parsedow(ctx, s.Days)
		if !ok {
			return nil, false
		}
		log.Debugf(ctx, "days of week: %#x", dow)
	}

	ev := &spec.Event{
		Name:             s.Name,
		EnableDaysOfWeek: dow,
		StartTimeOfDay:   start,
		DurationSec:      duration,
	}
	return ev, true
}

func inferLocation() string {
	link, err := os.Readlink("/etc/localtime")
	if err == nil {
		if strings.HasPrefix(link, "/usr/share/zoneinfo/") {
			return link[20:]
		}
	}

	return time.Local.String()
}
