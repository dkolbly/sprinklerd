package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/dkolbly/sprinklerd/spec"
	"github.com/golang/protobuf/proto"
)

var log = logging.New("relay")

var dev *Driver

const numChannels = 4

func main() {
	logging.SetHumanOutput(false, false, "DEBUG")
	logging.PrettyShowExtraFields()

	ctx := context.Background()
	Startup(logging.Set(ctx, "phase", "boot"))

	tx := make(chan transition, 2) // transition notification channel
	ch := make(chan int, 2)        // change notification channel
	s := &Relays{
		loc:     time.Local,
		sys:     &spec.System{},
		notify:  ch,
		transit: tx,
		scheds:  make(map[string]*Schedule),
	}
	for i := 0; i < 4; i++ {
		s.states[i] = State{
			ID: i,
		}
	}

	var err error

	dev, err = NewDriver()
	if err != nil {
		panic(err)
	}

	//go s.scheduled(ch)
	go s.processTransitions(tx)

	go func() {
		time.Sleep(20 * time.Millisecond)
		s.Configure(ctx, builtin())
	}()

	port := ":1060"
	log.Infof(ctx, "starting on %s", port)

	panic(http.ListenAndServe(port, s))
}

// see if we are currently in an event
func (s *Relays) active(ctx context.Context) bool {
	s.lock.Lock()
	defer s.lock.Unlock()

	t := time.Now().In(s.loc)

	for _, ev := range s.sys.Events {
		// any match is sufficient; it doesn't matter WHICH,
		// if there are multiple, events we are in, because the
		// next END transition logic will take care of that
		if ev.In(ctx, t) {
			// but log it for debugging purposes
			log.Debugf(ctx, "currently in event %q", ev)
			return true
		}
	}
	return false
}

type transition struct {
	at      time.Time
	relays  uint16
	subject *spec.Event
	state   bool
	ack     chan []State // optional ack channel
}

func (s *Relays) next(ctx context.Context, t time.Time) (*transition, int) {
	s.lock.Lock()
	defer s.lock.Unlock()

	var x *transition
	submit := func(ev *spec.Event, e *time.Time, state bool) {
		if e == nil {
			return
		}

		if x == nil || e.Before(x.at) {
			x = &transition{
				at:      *e,
				subject: ev,
				state:   state,
				relays:  0xf,
			}
		}
	}

	for _, ev := range s.sys.Events {
		submit(ev, ev.Next(ctx, t), true)
		submit(ev, ev.NextEnd(ctx, t), false)
	}

	return x, s.rev
}

type Relays struct {
	lock    sync.Mutex
	rev     int
	loc     *time.Location
	sys     *spec.System
	states  [numChannels]State
	notify  chan int
	transit chan transition
	scheds  map[string]*Schedule
}

type State struct {
	ID           int        `json:"id"`
	On           bool       `json:"on"`
	LastChange   *time.Time `json:"last_change,omitempty"`
	ChangeReason string     `json:"change_reason,omitempty"`
}

func (s *Relays) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// no performance necessary... lock the entire system for
	// every single request
	s.lock.Lock()
	defer s.lock.Unlock()

	ctx := logging.Set(r.Context(), "remote", r.RemoteAddr)
	log.Infof(ctx, "%s %s", r.Method, r.URL.String())
	r = r.WithContext(ctx)

	switch r.URL.Path {
	case "/configure":
		switch r.Method {
		case http.MethodPost:
			s.doConfig(w, r)
		default:
			MethodNotAllowed(w, r)
		}
		return
	case "/next":
		switch r.Method {
		case http.MethodGet:
			s.doNext(w, r)
		default:
			MethodNotAllowed(w, r)
		}
		return

	case "/demo1":
		switch r.Method {
		case http.MethodGet:
			getdemo(w, demo1())
		default:
			MethodNotAllowed(w, r)
		}
		return

	case "/demo2":
		switch r.Method {
		case http.MethodGet:
			getdemo(w, demo2())
		default:
			MethodNotAllowed(w, r)
		}
		return

	case "/relay/0", "/relay/1", "/relay/2", "/relay/3":
		switch r.Method {
		case http.MethodPut:
			s.doManual(w, r)
		case http.MethodGet:
			s.doStatusCheck(w, r)
		default:
			MethodNotAllowed(w, r)
		}
		return

	default:
		http.NotFound(w, r)
	}
}

func MethodNotAllowed(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusMethodNotAllowed)
	w.Write([]byte("{\"error\":\"method not allowed\"}\n"))
}

func writeError(ctx context.Context, w http.ResponseWriter, status int, err error) {

	w.WriteHeader(status)
	var tmp struct {
		Error string `json:"error"`
	}
	tmp.Error = err.Error()

	log.Errorf(ctx, "returning %d status: %s", status, err)

	buf, _ := json.Marshal(tmp)
	w.Write(append(buf, '\n'))
}

type NextInfo struct {
	Name      string    `json:"name"`
	FutureSec int       `json:"future_sec"`
	At        time.Time `json:"at"`
}

func (s *Relays) doNext(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	nexts := make([]NextInfo, 0, 5)

	loc, _ := time.LoadLocation(s.sys.Location)
	t := time.Now().In(loc)

	for _, ev := range s.sys.Events {
		n := ev.Next(ctx, t)
		if n != nil {
			nexts = append(nexts, NextInfo{
				Name:      ev.Name,
				FutureSec: int(n.Sub(t) / time.Second),
				At:        *n,
			})
		}
	}

	buf, _ := json.MarshalIndent(nexts, "", "  ")
	w.Write(append(buf, '\n'))
}

func (s *Relays) doConfig(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	defer r.Body.Close()
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}

	ptr := r.Header.Get("X-Omen-Ptr")
	if ptr != "" {
		ctx = logging.Set(ctx, "ptr", ptr)
	}

	var tmp spec.System
	err = proto.Unmarshal(buf, &tmp)
	if err != nil {
		writeError(ctx, w, http.StatusBadRequest, err)
		return
	}

	err = s.Configure(ctx, &tmp)
	if err != nil {
		writeError(ctx, w, http.StatusBadRequest, err)
		return
	}
}

func getdemo(w http.ResponseWriter, src *spec.System) {
	buf, _ := proto.Marshal(src)
	w.Write(buf)
}

func demo1() *spec.System {
	return &spec.System{
		Location: "America/New_York",
		Events: map[string]*spec.Event{
			"morning": &spec.Event{
				Name:             "morning",
				DurationSec:      20 * 60,
				EnableDaysOfWeek: 0x7f,
				StartTimeOfDay:   -4 * 3600, // 8 AM
			},
			"evening weekday": &spec.Event{
				Name:             "evening weekday",
				DurationSec:      20 * 60,
				EnableDaysOfWeek: 0x3e,     // M-F
				StartTimeOfDay:   7 * 3600, // 7 PM
			},
			"evening weekend": &spec.Event{
				Name:             "evening weekend",
				DurationSec:      30 * 60,
				EnableDaysOfWeek: 0x41,     // Sa, Su
				StartTimeOfDay:   8 * 3600, // 8 PM
			},
		},
	}
}

func demo2() *spec.System {
	return &spec.System{
		Location: "America/Chicago",
		Events: map[string]*spec.Event{
			"morning": &spec.Event{
				Name:             "morning",
				DurationSec:      20 * 60,
				EnableDaysOfWeek: 0x7f,
				StartTimeOfDay:   -4 * 3600, // 8 AM
			},
			"evening weekday": &spec.Event{
				Name:             "evening weekday",
				DurationSec:      20 * 60,
				EnableDaysOfWeek: 0x3e,     // M-F
				StartTimeOfDay:   7 * 3600, // 7 PM
			},
			"evening weekend": &spec.Event{
				Name:             "evening weekend",
				DurationSec:      30 * 60,
				EnableDaysOfWeek: 0x41,     // Sa, Su
				StartTimeOfDay:   8 * 3600, // 8 PM
			},
		},
	}
}

func (s *Relays) doStatusCheck(w http.ResponseWriter, r *http.Request) {
	channel := r.URL.Path[len(r.URL.Path)-1] - '0'

	if channel < 0 || channel >= numChannels {
		http.NotFound(w, r)
		return
	}
	out, _ := json.Marshal(&s.states[channel])
	w.Write(append(out, '\n'))
}

func (s *Relays) doManual(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	channel := r.URL.Path[len(r.URL.Path)-1] - '0'

	if channel < 0 || channel >= numChannels {
		http.NotFound(w, r)
		return
	}

	defer r.Body.Close()
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}

	data := strings.TrimSpace(string(buf))
	state, ok := manualops[data]
	if !ok {
		writeError(ctx, w, http.StatusBadRequest, invalidOp(data))
		return
	}
	log.Debugf(ctx, "manual operation on channel %c : set %t",
		channel, state)

	ack := make(chan []State)

	s.transit <- transition{
		at: time.Now(),
		subject: &spec.Event{
			Name: "manual",
		},
		state:  state,
		ack:    ack,
		relays: 1 << channel,
	}

	now := <-ack
	if len(now) != 1 {
		panic("expected exactly one ack")
	}
	out, _ := json.Marshal(&now[0])
	w.Write(append(out, '\n'))
}

type invalidOp string

func (i invalidOp) Error() string {
	return fmt.Sprintf("%q is not a recognized manual operation", string(i))
}

var manualops = map[string]bool{
	"1":     true,
	"true":  true,
	"on":    true,
	"yes":   true,
	"0":     false,
	"false": false,
	"off":   false,
	"no":    false,
}

func hot() *spec.System {
	return &spec.System{
		Location: "America/Chicago",
		Events: map[string]*spec.Event{
			"morning": &spec.Event{
				Name:             "morning",
				DurationSec:      4 * 60,
				EnableDaysOfWeek: 0x7f,
				StartTimeOfDay:   2*3600 + 50*60,
			},
			"afternoon": &spec.Event{
				Name:             "afternoon",
				DurationSec:      4 * 60,
				EnableDaysOfWeek: 0x41, // Sa, Su
				StartTimeOfDay:   2*3600 + 52*60,
			},
		},
	}
}

func builtin() *spec.System {
	data, err := base64.StdEncoding.DecodeString(encodedBuiltin)
	if err != nil {
		panic(err)
	}
	tmp := new(spec.System)
	err = proto.Unmarshal(data, tmp)
	if err != nil {
		panic(err)
	}
	return tmp
}

var encodedBuiltin = `
Cg9BbWVyaWNhL0NoaWNhZ28SHQoHRXZlbmluZxISCgdFdmVuaW5nELAJGH8g4IkDEh0KB01vcm5p
bmcSEgoHTW9ybmluZxCIDhh/IJ+ZAg==
`
