package main

import (
	"context"
	"time"

	"bitbucket.org/dkolbly/sprinklerd/spec"
)

func (s *Relays) Configure(ctx context.Context, sys *spec.System) error {
	loc, err := time.LoadLocation(sys.Location)
	if err != nil {
		log.Errorf(ctx, "could not load location %q", sys.Location)
		return err
	}
	log.Debugf(ctx, "Configuring system in location %s with %d events", sys.Location, len(sys.Events))
	log.Debugf(ctx, "the time is now %s", tform(time.Now().In(loc)))

	s.loc = loc
	s.sys = sys
	s.rev++

	// notify the scheduler that the rev has changed
	//s.notify <- s.rev

	// create new schedules, update schedules, and cancel schedules

	updated := make(map[string]*spec.Event)
	for _, ev := range sys.Events {

		if ev.EnableDaysOfWeek == 0 {
			// ignore it... it is dead to us
			log.Debugf(ctx, "ignoring schedule %q", ev.Name)
			continue
		}

		updated[ev.Name] = ev
		sch := s.scheds[ev.Name]
		if sch == nil {
			// it's a brand new schedule
			log.Debugf(ctx, "creating schedule %q", ev.Name)
			s.scheds[ev.Name] = NewSchedule(s.loc, ev, s.transit)
		} else {
			log.Debugf(ctx, "updating schedule %q", ev.Name)
			sch.Update(ev)
		}
	}

	// clean up schedules that weren't mentioned
	for k, sch := range s.scheds {
		if updated[k] == nil {
			log.Debugf(ctx, "canceling schedule %q", k)
			sch.Cancel()
			delete(s.scheds, k)
		}
	}
	return nil
}
