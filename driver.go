package main

import (
	"context"
	"time"

	"bitbucket.org/dkolbly/logging"
)

var txlog = logging.New("tx")

func (s *Relays) processTransitions(txs <-chan transition) {
	ctx := context.Background()
	asks := make(map[string]uint16)

	for tx := range txs {
		s.apply(ctx, tx, asks)
	}
}

func (s *Relays) apply(ctx context.Context, tx transition, asks map[string]uint16) {
	ctx = logging.Set(ctx, "ev", tx.subject.Name)

	if tx.subject.Name != "manual" {
		if tx.state {
			asks[tx.subject.Name] = tx.relays
			txlog.Debugf(ctx, "set asks[%s] to %04x", tx.subject.Name, tx.relays)
		} else {
			delete(asks, tx.subject.Name)
			txlog.Debugf(ctx, "remove asks[%s]", tx.subject.Name)
		}
	} else {
		txlog.Debugf(ctx, "transition %04x to %t (manual mode)", tx.relays, tx.state)
	}

	var total uint16

	for k, contrib := range asks {
		total |= contrib
		log.Debugf(ctx, "accum relay state: %04x after %s", total, k)
	}
	if tx.state { // this only has an effect in MANUAL mode; other ones are in the asks map
		total |= tx.relays
		log.Debugf(ctx, "accum relay state: %04x after TX %s", total, tx.subject.Name)
	}

	var touch []State

	t := time.Now().In(s.loc)

	for i := 0; i < numChannels; i++ {
		// figure out what state we WANT it to be in
		goal := total&(1<<uint(i)) != 0

		// then see if the state is changing
		if s.states[i].On != goal {
			txlog.Debugf(ctx, "relay[%d] changed to %t", i, goal)
			// it's actually a change
			s.states[i] = State{
				ID:           i,
				On:           goal,
				LastChange:   &t,
				ChangeReason: tx.subject.Name,
			}
		} else {
			txlog.Debugf(ctx, "relay[%d] was already %t", i, goal)
		}
		if tx.relays&(1<<uint(i)) != 0 {
			// only report the status one the ones we are supposedly affecting
			touch = append(touch, s.states[i])
		}
	}
	if dev != nil {
		err := dev.Set(uint8(total))
		if err != nil {
			log.Errorf(ctx, "error setting %#x : %s", err)
		}
	}
	if tx.ack != nil {
		tx.ack <- touch
	}
}
